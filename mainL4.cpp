/*Tema 3
    Nemes Ilinca
    Master CSC
*/
/*
    Sortarea unui heap folosind metodele Quick Sort si Heap Sort
    Atat HeapSort cat si QuickSort au aceeasi complexitate O(nlgn).
    Graficul la HeapSort este aproape liniar si crescator. La Qucik Sort graficul tinde sa arate liniar, cu interferente, dar tot crescator.
    Referitor la eficienta celor doi algoritmi, Quick sort este mai eficient decat Heap sort, deoarece efectueaza mai putine operatii,
    deci este mai rapid.
*/

#include <iostream>
#include "Profiler.h"

Profiler profiler("Mediu");

int k;

//Bottom-up
void MAX_HEAPIFY(int *a, int i,int n)
{
	int ind, aux;
		if ((2*i + 1 )< n)
		{
			ind = i;
			if (2 * i + 2 < n)
			{
				profiler.countOperation("Op_HeapSort", k, 1);
				if (a[2 * i + 2] > a[ind])
					ind = 2 * i + 2;
			}
			profiler.countOperation("Op_HeapSort", k, 1);
			if (a[2 * i + 1 ]>= a[ind])
				ind = 2 * i + 1;
			if (ind != i)
			{
					profiler.countOperation("Op_HeapSort", k, 3);
					aux = a[i];
					a[i] = a[ind];
					a[ind] = aux;
					MAX_HEAPIFY(a, ind, n);
				}
		}
}


void BUILD_MAX_HEAP_BU(int *a, int n)
{
	int i;
	for (i = (n-1) / 2; i >= 0; i--)
	{
		MAX_HEAPIFY(a, i,n);
	}
}

//Heap_sort
void Heap_sort(int *a, int n)
{	int i,aux, dim=n;
	BUILD_MAX_HEAP_BU(a, n);

	for (i = n - 1; i > 0; i--)
	{
	    profiler.countOperation("Op_HeapSort", k, 4);
		aux = a[0];
		a[0] = a[i];
		a[i] = aux;
		dim--;
		MAX_HEAPIFY(a, 0, dim);
	}
}

//QuickSort
void swap ( int *x,int *y)
{
  int temp=*x;
  *x=*y;
  *y=temp;
}

int partitionare(int arr[], int low, int high, int n)

{
    // pivot (Element to be placed at right position)
   int pivot = arr[high];

    int i = (low - 1);  // Index of smaller element
    profiler.countOperation("Op_QuickSort", n, 1);
    for (int j = low; j <= high- 1; j++)
    {
        profiler.countOperation("Op_QuickSort", n, 1);
        if (arr[j] < pivot)
        {
            i++;    // increment index of smaller element
            profiler.countOperation("Op_QuickSort", n, 3);
            swap (&arr[i],&arr[j]);
        }
    }
    profiler.countOperation("Op_QuickSort", n, 3);
    swap (&arr[i + 1], &arr[high]);
    return (i + 1);
}

void Quick_Sort(int a[], int low, int high, int n)
{
    if(low<high)
    {
        int p;
        p=partitionare(a, low, high, n);
        Quick_Sort(a, low, p-1, n);
        Quick_Sort(a, p+1, high, n);
    }
}

int Quick_Select(int a[], int k, int low, int high, int n)
{
	if (low == high) return low;
	int pivot = partitionare(a, low, high, n);
	int crtRank = pivot-low+1;
	if (k == crtRank)
		return a[pivot];
	else if (k < crtRank)
		high = pivot - 1;
	else {
            low = pivot + 1;
            k -= crtRank;
	}
	return Quick_Select(a, k, low, high, n);
}

//defavorabil
int partitionare_defav(int arr[], int low, int high, int n)
{
    int i, j, pivot;
	i =low-1;
	pivot = arr[high];
	profiler.countOperation("Op_QuickSort_Defav", n, 1);
	for (j =low; j <high; j++)
	{
		profiler.countOperation("Op_QuickSort_Defav", n, 1);
		if (arr[j] <= pivot)
		{
			i++;
			profiler.countOperation("Op_QuickSort_Defav", n, 3);
			swap(&arr[i], &arr[j]);
		}
	}
	profiler.countOperation("Op_QuickSort_Defav", n, 3);
	swap(&arr[i+1], &arr[high]);
	return i + 1;
}

void Quick_Sort_defav(int arr[], int low, int high, int n)
{
	if (low < high)
	{
		int p;
		p =partitionare_defav(arr, low, high, n);
		Quick_Sort_defav(arr, low, p - 1, n);
		Quick_Sort_defav(arr, p + 1, high, n);
	}
}

//favorabil
int partitionare_fav(int a[], int low, int high, int n)
{
	int i, j, pivot;
	i =low-1;
	pivot = a[high];
	profiler.countOperation("Op_QuickSort_Fav", n, 1);
	for (j = low; j < high; j++)
	{
		profiler.countOperation("Op_QuickSort_Fav", n, 1);
		if (a[j] <= pivot)
		{
			i++;
			profiler.countOperation("Op_QuickSort_Fav", n, 3);
			swap(&a[i], &a[j]);
		}
	}
	profiler.countOperation("Op_QuickSort_Fav", n, 3);
	swap(&a[i+1], &a[high]);
	return i + 1;
}

void Quick_Sort_fav(int a[], int low, int high, int n)
{
	if (low < high)
	{
		int p;
		p =partitionare_fav(a, low, high, n);
		Quick_Sort_fav(a, low, p - 1, n);
		Quick_Sort_fav(a, p + 1, high, n);
	}
}
using namespace std;

int main()
{
    int i;
    int A[10] = { 4, 1, 3, 2, 16, 9, 10, 14, 8, 7 };
    int B[10] = { 4, 1, 3, 2, 16, 9, 10, 14, 8, 7 };
    int C[10] = { 10, 4, -25, 25, 0, 8, 2, 12, 3, 1 };
	int	n = 10;

	//Verificare corectitudine algoritm
	int pozitie = Quick_Select(C, 10, 0, n-1, n);
	printf("Pozitia elementului 10 este %d.", pozitie);

	Quick_Sort(A, 0, n-1, n);
	printf("\nA sortat(quick sort):  ");
	for (i=0; i<n; i++)
	{
	    printf("%d ", A[i]);
	}

	Heap_sort(B, n);
	printf("\nB sortat(heap sort): ");
	for(int i=0; i<n; i++)
    {
        printf("%d ", B[i]);
    }


    //Caz mediu
    int v[10000], cv[10000], m[10000], t[10000];
	for (k = 100; k < 10000; k = k + 100)
	{
		FillRandomArray(v, k, 0, 3000, false, 0);
		for (i = 0; i < k; i++)
			cv[i] = v[i];
		Heap_sort(v, k);
		Quick_Sort(cv, 0, k-1, k);
	}
    profiler.divideValues("Op_HeapSort", 5);
	profiler.divideValues("Op_QuickSort", 5);
	profiler.createGroup("Operatii_mediu", "Op_HeapSort", "Op_QuickSort");
	profiler.reset("QuickSort");

	//caz mediu, defav, fav quick sort
	for (k = 100; k < 10000; k = k + 100)
	{
		FillRandomArray(v, k, 0, 3000, false, 1);
		for (i = 0; i < k; i++)
		{
			cv[i] = v[i];
		}
		Quick_Sort_fav(v, 0, k-1, k);
		Quick_Sort_defav(v, 0, k-1, k);
		FillRandomArray(t, k, 0, 3000, false, 0);
		for (i = 0; i < k; i++)
		{
			m[i] = t[i];
		}
		Quick_Sort(t, 0, k - 1, k);
		Heap_sort(m, k);
	}

	profiler.createGroup("Operatii_mediu", "Op_HeapSort", "Op_QuickSort");
	profiler.createGroup("Operatii_quick_sort", "Op_QuickSort_Fav", "Op_QuickSort_Defav", "Op_QuickSort");
	profiler.showReport();

    return 0;
}
