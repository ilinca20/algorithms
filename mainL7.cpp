#include <iostream>
#include "Profiler.h"

Profiler profiler("Cazul 1");

int E; //numar muchii
int V; //numar noduri
int caz; //caz=0 cand

typedef struct nod{
    int val;
    struct nod* next;
}nod;

enum{
    ALB, NEGRU, GRI
};

typedef struct coada{
    nod* first;
    nod* last;
}coada;

typedef struct{
    int v; //numar de varfuri
    int **t; //tabloul de liste
    int *cul;
    int *p;
    int *d; //timpul de descoperire
}graf;

void count_operation(int i)
{
    if (caz == 0) profiler.countOperation("BFS_numar_noduri_constant", E, i);
    if (caz == 1) profiler.countOperation("BFS_numar_muchii_constant", V, i);
}


using namespace std;

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
