/* Tema laborator 6
    Nemes Ilinca
    Master CSC
*/
/*
Cerinta: Statistici dinamice de ordine
    OS_SELECT :O(logn) BUILD_TREE: O(n)
    Graficul obtinut este liniar, o data cu cresterea numarului de elemene din arbore si numarul de operatii efectuate
asupra acestora creste.
*/

#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"
Profiler profiler("Statistici");

#define COUNT 10
int NR;
typedef struct nod
{
	int dim;
	int v;
	struct nod *st;
	struct nod *dr;
}nod;

nod* CONSTR_ARB(int st, int dr)
{
	int m;
	nod *x;
	x = (nod*)malloc(sizeof(nod));
	if (st <= dr)
	{
		m = (st + dr) / 2;
		x->v = m;
		x->dim = dr - st + 1;
		//printf("\n %d=%d (%d,%d)", m, x->dim,st,dr);
		x->st = CONSTR_ARB(st, m - 1);
		x->dr = CONSTR_ARB(m + 1, dr);
		return x;
	}
	else
		return NULL;
}

void inorder(nod * root)
{
	if (root != NULL)
	{

		inorder(root->st);
		printf("  %d->dim=%d ", root->v, root->dim);
		inorder(root->dr);
	}
}
void nr_op(int i)
{
	if (NR>0)
		profiler.countOperation("Operatii", NR,i);
}

nod* OS_SELECT(nod *x, int i)
{
	int r=1;
	nr_op(1);
	if (x->st == NULL){ r = 1; }
	else
	{
		r = x->st->dim + 1;
	}

	if (i == r)
	{
		return x;
	}
	else
	{
		if (i < r)
		{
			return OS_SELECT(x->st, i);
		}
		else
			return OS_SELECT(x->dr, i - r);
	}
}

void print(nod *rad, int spatiu)//pretty print
{
	int i,j=COUNT;
	if (rad == NULL)
		return;

	spatiu += COUNT;
	print(rad->dr, spatiu);
	printf("\n");

	for (i = j; i<spatiu; i++)
		printf(" ");
	printf("%d[%d]\n", rad->v,rad->dim);
	print(rad->st, spatiu);
}


int main()
{
//	int i, j, n;
	int k;
	nod *rad;
//	int val[10000];
	rad = (nod*)malloc(sizeof(nod));
	nod *select = (nod*)malloc(sizeof(nod));
	rad = NULL;
	select = NULL;

	//Demo n=11
	printf("Datele de intrare sunt numerele de la 1 la 11\nAfisarea in inordine a arborelui:\n");
	rad = CONSTR_ARB(1,11);
	inorder(rad);
	NR = -1;
	k=rad->dim;
    select = OS_SELECT(rad, k);
    printf(" \nPozitia %d: Element=%d", k, select->v);

	print(rad, 10);


/*
	for (n= 100; n <= 10000; n = n + 100)
	{
		NR = n;
		for (i = 1; i <= 5; i++)
		{
		free(rad);
			rad = CONSTR_ARB(1,n+1);
			inorder(rad);
			FillRandomArray(val, n, 1,n, true, 0);
			for (j = 0; j < n; j++)
			{
				k =1+ val[t] % rad->dim;
				select = OS_SELECT(rad, k);
				if (select)
				printf(" \npozitia %d: element=%d", k, select->v);
            }
        }
	}

	//profiler.showReport();
*/
	return 0;
}
