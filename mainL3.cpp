#include <stdio.h>
#include <stdlib.h>

#include "Profiler.h"
Profiler profiler("Mediu");
/* Tema laborator 3
    Nemes Ilinca
    Master CSC
*/
/*
Cerinta: Compararea metodelor bottom-up si top-down de constructie de heap + heap-sort
    In cazul mediu statistic se observa ca graficele au o forma aproximativ lineara, crescatoare. La toate dintre cele 5
masuratori graficele se gasesc aproximativ in aceasi pozitie fata de axe cat si unul fata de celalalt.
    Numarul de operatii efectuate de metoda Bottom-up este semnificativ mai mic decat in cazul metodei Top-Down,
deci metoda Bottom-Up este mai eficienta.
    In cazul defavorabil graficele tind sa fie chiar drepte.
    Metoda Top-Down este eficienta la utilizare atunci cand sirului i se mai adauga elemente ulterior, iar metoda Bottom-Up
e mai utila cand lungimea si elemenetele sirului nu se mai modifica.
*/



int k;

//Bottom-up
void MAX_HEAPIFY(int *a, int i,int n)
{
	int ind, aux;
		if ((2*i + 1 )< n)
		{
			ind = i;
			if (2 * i + 2 < n)
			{
				profiler.countOperation("Op_BU", n,1);
				if (a[2 * i + 2] > a[ind])
					ind = 2 * i + 2;
			}
			profiler.countOperation("Op_BU", n,1);
			if (a[2 * i + 1 ]>= a[ind])
				ind = 2 * i + 1;
			if (ind != i)
			{
					profiler.countOperation("Op_BU", n,3);
					aux = a[i];
					a[i] = a[ind];
					a[ind] = aux;
					MAX_HEAPIFY(a, ind, n);
				}
		}
}


void BUILD_MAX_HEAP_BU(int *a, int n)
{
	int i;
	for (i = (n-1) / 2; i >= 0; i--)
	{

		MAX_HEAPIFY(a, i,n);
	}
}

//Top-down
void HPush(int*a, int*m, int val)
{
	int i,aux;
	a[*m] = val;
	profiler.countOperation("Op_TD", k,1);
	i = *m;
	profiler.countOperation("Op_TD", k,1);
	while (a[i]>a[(i+1)/2-1] && i>0)// a[i]>parintele lui
	{
		profiler.countOperation("Op_TD", k,3);
		aux = a[i];
		a[i] = a[(i + 1) / 2 - 1];
		a[(i + 1) / 2 - 1] = aux;
		i = (i + 1) / 2 - 1;
	}
	(*m)++;
}


void BUILD_MAX_HEAP_TD(int* a, int* m, int * b, int n)
{
	int i;
	for (i = 0; i < n; i++)
		HPush(a,m, b[i]);
}

//Heap_sort
void Heap_sort(int *a, int *n)
{	int i,aux, dim=*n;
	BUILD_MAX_HEAP_BU(a, *n);

	for (i = (*n) - 1; i > 0; i--)
	{
		aux = a[0];
		a[0] = a[i];
		a[i] = aux;
		dim--;
		MAX_HEAPIFY(a, 0, i);
	}
}



int main()
{
	int i, A[10] = { 4, 1, 3, 2, 16, 9, 10, 14, 8, 7 };
	int	n = 10,m;

	//Verificare corectirudine algoritm

	printf("Sir: ");
	for (i = 0; i < n; i++)
		printf("%d ", A[i]);

	printf("\nBU->A: ");
	BUILD_MAX_HEAP_BU(A, n);
	for (i = 0; i < n; i++)
		printf("%d ", A[i]);

	m = 0;
	int B[100], C[10] = { 4, 1, 3, 2, 16, 9, 10, 14, 8, 7 };
	BUILD_MAX_HEAP_TD(B, &m,C,n);
	printf("\nTD->B: ");
	for (i = 0; i < n; i++)
		printf("%d ", B[i]);

	int cn = n;
	Heap_sort(A, &cn);
	printf("\nHEAP_SORT->A: ");
	for (i = 0; i < n; i++)
		printf("%d ", A[i]);


	cn = m;
	Heap_sort(B, &cn);
	printf("\nHEAP_SORT->B: ");
	for (i = 0; i < m; i++)
		printf("%d ", B[i]);



	//Caz mediu-statistic

	int v[10000],t[10000],cv[10000];
	for (k = 100; k < 10000; k = k + 100)
	{
		m = 0;
		FillRandomArray(v, k, 0, 300, false, 0);
		for (i = 0; i < k; i++)
			cv[i]=v[i];
		BUILD_MAX_HEAP_BU(cv, k);
		BUILD_MAX_HEAP_TD(t, &m, v, k);
	}
	profiler.divideValues("Op_TD", 5);
	profiler.divideValues("Op_BU", 5);
	profiler.createGroup("Operatii_mediu", "Op_TD", "Op_BU");
	profiler.reset("Defavorabil");



	//Caz defavorabil
	int x[10000], y[10000], z[10000];
	for (k = 100; k < 10000; k = k + 100)
	{
		m = 0;
		FillRandomArray(x, k, 0, 300, false, 2);
		for (i = 0; i < k; i++)
			z[i] = x[i];
		BUILD_MAX_HEAP_BU(z, k);
		BUILD_MAX_HEAP_TD(y, &m, x, k);
	}
	profiler.createGroup("Operatii_defavorabil", "Op_TD", "Op_BU");


	profiler.showReport();

	getchar();


	return 0;
}
