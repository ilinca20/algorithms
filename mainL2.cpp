/*Tema laborator 2
    Nemes Ilinca-Daniela
    Master CSC
*/

/*
BubbleSort(A, n)
    swaps = true
    size = n
    while(swaps)
        swaps = false
        for i = 1 to n-1
            if A[i]>A[i+1] then
                swap(A[i], A[i+1])
                swaps =true
        size = size-1

*/

/*
InsertionSort(A, n)
    for j=2 to n
        key <- A[j]
        j <- i-1
        while i>0 and A[i]> key
            A[i+1] <- A[i]
            i <- i-1
        A[j+1] <- key
*/

/*
SelectionSort(A, n)
    for i <- 1 to n-1
        min <-i
        for j <- i+1 to n
            if A[j] < A[min] then
                min <- j
        if min!=i then interchange A[i] and A[min]

*/

#include <iostream>
#include <stdio.h>
#include "Profiler.h"

Profiler profiler("Mediu");

void BubbleSort(int* A, int n)
{
    int size = n, aux;
    bool swaps = true;
    Operation BScomp=profiler.createOperation("BubbleComparisons", n);
    Operation BSatrib=profiler.createOperation("BubbleAssignments", n);
    while(swaps)
    {
        swaps = false;
        for(int k=0; k<size-1; k++)
            for (int i=0; i<n-1; i++)
            {
                BScomp.count();
                if (A[i] > A[i+1])
                {
                    aux=A[i];
                    A[i]=A[i+1];
                    A[i+1]=aux;
                    BSatrib.count(3);
                    swaps=true;
                }
            }
    }
}

void print(int* A, int n)
{
    for (int i=0; i<n; i++)
        printf("%d ", A[i]);
    printf("\r\n");
}

//Selection Sort
void swap(int *x, int *y)
{
    int temp=*x;
    *x=*y;
    *y=temp;
}

void SelectionSort(int* A, int n)
{
    int min;
    Operation SScomp=profiler.createOperation("SelectionComparisons", n);
    Operation SSatrib=profiler.createOperation("SelectionAssignments", n);
    for (int i=0; i<=n-1; i++)
    {
        min=i;
        for (int j=i+1; j<n; j++)
        {
            SScomp.count();
        if (A[j]<A[min])
                min=j;
        }
        swap(&A[min], &A[i]);
        SSatrib.count(3);

    }
}

//Insertion Sort
void InsertionSort(int* A, int n)
{
    int key, i, j;
    Operation IScomp=profiler.createOperation("InsertionComparisons", n);
    Operation ISatrib=profiler.createOperation("InsertionAssignments", n);
    for (i=2; i<n; i++)
    {
        key=A[i];
        ISatrib.count();
        j=i-1;
        IScomp.count();
        while (j>=0 && A[j]>key)
        {
            A[j+1]=A[j];
            j=j-1;
            ISatrib.count();
        }
        A[j+1]=key;
        ISatrib.count();
    }
}

int main()
{
    /*
    //testare algoritm
    int A[10]= {6,7,2,4,9,1,3,8,2,6};
    print(A, 10);
    SelectionSort(A, 10);
    print(A, 10);
*/
    int B[10000];
    int C[10000];
    int D[10000];
    int n=100;

    //caz mediu statistic
    for(int k=0; k<5; k++){
        for (; n<=10000; n+=100)
        {
            printf("%d \r\n", n);
            FillRandomArray(B, n);
            for (int i=0; i<n; i++)
            {
                C[i]=B[i];
                D[i]=C[i];
            }
            BubbleSort(B, n);
            SelectionSort(C, n);
            InsertionSort(D, n);
        }
    }

    profiler.addSeries("BubbleOperations", "BubbleComparisons", "BubbleAssignments");
    profiler.addSeries("SelectionOperations", "SelectionComparisons", "SelectionAssignments");
    profiler.addSeries("InsertionOperations", "InsertionComparisons", "InsertionAssignments");

    profiler.divideValues("BubbleOperations", 5);
    profiler.divideValues("BubbleComparisons", 5);
    profiler.divideValues("BubbleAssignments", 5);
    profiler.divideValues("SelectionOperations", 5);
    profiler.divideValues("SelectionComparisons", 5);
    profiler.divideValues("SelectionAssignments", 5);
    profiler.divideValues("InsertionOperations", 5);
    profiler.divideValues("InsertionComparisons", 5);
    profiler.divideValues("InsertionAssignments", 5);

    profiler.createGroup("Operations", "BubbleOperations", "SelectionOperations", "InsertionOperations");
    profiler.createGroup("Comparisons", "BubbleComparisons", "SelectionComparisons", "InsertionComparisons");
    profiler.createGroup("Assignments", "BubbleAssignments", "SelectionAssignments", "InsertionAssignments");


    profiler.reset("Defavorabil");


    //caz defavorabil
    for (; n<=10000; n+=100)
        {
            printf("%d \r\n", n);
            for (int i=0; i<n; i++)
            {
              B[i]=n-i-1;
              C[i]=n-i-1;
              D[i]=n-i-1;
            }
            BubbleSort(B, n);
            SelectionSort(C, n);
            InsertionSort(D, n);
        }
    profiler.addSeries("BubbleOperations", "BubbleComparisons", "BubbleAssignments");
    profiler.addSeries("SelectionOperations", "SelectionComparisons", "SelectionAssignments");
    profiler.addSeries("InsertionOperations", "InsertionComparisons", "InsertionAssignments");

    profiler.createGroup("Operations", "BubbleOperations", "SelectionOperations", "InsertionOperations");
    profiler.createGroup("Comparisons", "BubbleComparisons", "SelectionComparisons", "InsertionComparisons");
    profiler.createGroup("Assignments", "BubbleAssignments", "SelectionAssignments", "InsertionAssignments");


    profiler.reset("Favorabil");

    //favorabil
    for (; n<=10000; n+=100)
        {
            printf("%d \r\n", n);
            for (int i=0; i<n; i++)
            {
              B[i]=i;
              C[i]=i;
              D[i]=i;
            }
            BubbleSort(B, n);
            SelectionSort(C, n);
            InsertionSort(D, n);
        }
    profiler.addSeries("BubbleOperations", "BubbleComparisons", "BubbleAssignments");
    profiler.addSeries("SelectionOperations", "SelectionComparisons", "SelectionAssignments");
    profiler.addSeries("InsertionOperations", "InsertionComparisons", "InsertionAssignments");

    profiler.createGroup("Operations", "BubbleOperations", "SelectionOperations", "InsertionOperations");
    profiler.createGroup("Comparisons", "BubbleComparisons", "SelectionComparisons", "InsertionComparisons");
    profiler.createGroup("Assignments", "BubbleAssignments", "SelectionAssignments", "InsertionAssignments");

    profiler.showReport();
    return 0;
}
