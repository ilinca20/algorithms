/* Tema laborator 4
    Nemes Ilinca
    Master CSC
*/
/* Cerinta: Cautarea in tabelele de dispersie
    Pentru factorul de umplere egal cu 0.95 si 0.99 cresterea acestor doua valori este mai evidenta. Iesirea din
algoritmul de cautare se face fie cand s-a verificat fiecare element din tabela, fie cand s-a ajuns la o pozitie goala,
deoarece tabela este  completata in proportie de 95%, respectiv 99% numarul pozitiilor libere este mult mai mic, astfel
efectuandu-se mai multe apeluri la functia de hash.
    Valoarea pentru efortul maxim in cazul cautarii elementelor existente in tabela creste in unele cazuri mai brusc cand
factorul de umplere are valoarea de 0.99, deoarece tabela fiind aproape plina elementul cautat e posibil sa fi fost inserat
dupa mai multe iteratii ale lui i.
*/

#include <stdio.h>
#include <stdlib.h>
#include "Profiler.h"
#define N 9973
#define C1 1
#define C2 1


int nr_op_gasite, nr_op_negasite, tip_cautare, max_gasite, max_negasite;
//tip_cautare=0 cand numaram operatiile pt cautarile elementelor care sunt in hashTable op_gasite
//tip_cautare=1 cand numaram operatiile pt cautarile elementelor care nu sunt in hashTable op_negasite

int hash(int k, int i,int n)
{
	return (k%n+C1*i+C2*i*i)%n;
}

void numarare_op()
{
	if (tip_cautare == 0)
		nr_op_gasite++;
	else nr_op_negasite++;
}

int insert_hash( int *a, int val, int n)
{
	int i, j;
	i=0;
	while (i < n)
	{
		j = hash(val, i,n);
		if (a[j]==NULL)
		{
			a[j] = val;
			return j;
		}
		else i++;
	}

	return NULL;
}

int cautare( int *a, int k, int n)
{
	int i=0, j, m=0;
	do
	{
		numarare_op();
		m++;
		j = hash(k,i,n);
		if (a[j] == k)
		{
			if (m > max_gasite) max_gasite = m;
			return j;
		}
		 i++;
	}
	while (i <= n && a[j] !=NULL);
	if (m > max_negasite) max_negasite = m;
	return -1;
}


void afisare(int*a)
{
	int i;
	for (i = 0; i < N; i++)
	{
		if (a[i] == NULL) printf("NULL\n");
		else printf("%d  ", a[i]);
	}
}

void init(int *a)
{
	int i;
	for (i = 0; i < N; i++)
	{
		a[i] = NULL;
	}
}

void verificare()
{
	int v1[51],x,val1[51],m=51;
	for (x = 0; x < 51; x++)
		v1[x] = NULL;
	FillRandomArray(val1, 51, 20, 100, true, 0);
	printf("Valorile care vor fi inserate: ");
	for (x = 0; x < 48; x++)
		printf("%d ", val1[x]);
	printf("\n");
	for (x = 0; x < 48; x++)
		insert_hash(v1, val1[x], m);
	for (x = 0; x < 51; x++)
	{
		if (v1[x] == NULL) printf("NULL\n");
		else printf("v[%d]=%d   ",x, v1[x]);
	}
	printf("\ncauta 31=>%d",cautare(v1, 31, 51));
	printf("\ncauta %d=>%d\ncauta %d=>%d",val1[1] ,cautare(v1, val1[1], 51),val1[26] ,cautare(v1,val1[26],51));

}
int main()
{
	verificare();
	printf("\n\n");

	char s[] = "Efort mediu gasite";
	char s1[] = "Efort maxim gasite";
	char s2[] = "Efort mediu negasite";
	char s3[] = "Efort maxim negasite";
	printf("Factorul de umplere%21s%21s%23s%23s", s, s1, s2, s3);
	int t[N];
	int i,val[10000],m,k=3000,j,val1[10000],g;
	double fu[5] = { 0.8, 0.85, 0.9, 0.95, 0.99 };
	for (j = 0; j < 5; j++)
	{
		max_gasite = 0;
		max_negasite = 0;
		nr_op_gasite = 0;
		nr_op_negasite = 0;
		tip_cautare = 0;
		init(t);
		m = (int)(N*fu[j]);
		FillRandomArray(val, m, 0, 10000, false, 0);
		for (i = 0; i < m; i++)
			insert_hash(t, val[i],N);

		for (g = 0; g < 5; g++)
		{
			tip_cautare = 0;
			for (i = (k/2)*g; i < (k / 2)*(g+1); i++)
			{
				int p = N;
				cautare(t, val[i], p);
			}
			tip_cautare = 1;
			FillRandomArray(val1, k, 10001,40000, false, 0);
			for (i = 0; i <= k / 2; i++)
			{
				cautare(t, val1[i],N);
			}
		}
		printf("\n       %.2f%22.2f%19d%26.2f%20d", fu[j], (float)(nr_op_gasite) / 7500, max_gasite,(float) nr_op_negasite / 7500,max_negasite );
	}
	getchar();
	return 0;
}
